<?php 
// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_company']);
     unset($fields['billing']['billing_adress_1']);
     unset($fields['billing']['billing_state']);
     $fields['billing']['Otchestvo'] = array(
				    'label'     => __('lul', 'woocommerce'),
				    'placeholder'   => _x('lul', 'placeholder', 'woocommerce'),
				    'required'  => false,
				    'class'     => array('form-row-wide'),
				    'clear'     => true
				     );
     return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );





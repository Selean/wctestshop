<?php get_header(); ?> 
<?php 
    if ( have_posts() ) {
    	the_post();
    }
?>
 <!-- begin container  -->
        <div class="content container">

            <!-- begin breadcrumbs  -->
            <div class="breadcrumbs">
                <ul>
                    <li><a href="">Главная</a></li>
                    <li><a href="">Каталог</a></li>
                    <li><a href="">Города, улицы</a></li>
                    <li><?php echo $product->get_title();?></li>
                </ul>
            </div>
            <!-- end breadcrumbs -->

            
            <?php get_sidebar(); ?>
            <!-- begin product  -->
            <div class="product content">
            
            			
                <div class="product__image">
                    <?php echo $product->get_image('full');?>
                </div>
                <div class="product__title"><?php	do_action( 'woocommerce_shop_loop_item_title' ); ?></div>
                <div class="product__container group">
                    <button class="btn btn_blue product__btn-like" type="button">Нравится этот товар</button>
                    <a class="btn product__btn-manual" href="">Инструкция по оклейке</a>
                    <div class="social-menu social-menu_product">
                        <span class="social-menu__title">Поделиться</span>
                        <ul>
                            <li><a href="" target="_blank"><img src="" alt=""><img src="<?php bloginfo('template_url'); ?>/images/social/vk.png" alt=""></a></li>
                            <li><a href="" target="_blank"><img src="" alt=""><img src="<?php bloginfo('template_url'); ?>/images/social/f.png" alt=""></a></li>
                            <li><a href="" target="_blank"><img src="" alt=""><img src="<?php bloginfo('template_url'); ?>/images/social/ok.png" alt=""></a></li>
                            <li><a href="" target="_blank"><img src="" alt=""><img src="<?php bloginfo('template_url'); ?>/images/social/twitter.png" alt=""></a></li>
                            <li><a href="" target="_blank"><img src="" alt=""><img src="<?php bloginfo('template_url'); ?>/images/social/google.png" alt=""></a></li>
                        </ul>
                    </div>
                </div>
                <div class="product-items">
                    <div class="product__item action">                        
                        <div class="product__article">Артикул: <?php echo $product->get_sku();?></div>
                        <div class="product__size">Размер: <?php echo $product->get_attribute('size'); ?> </div>
                        <div class="product__number-bands">Количество полос: <?php echo $product->get_attribute('stripcount'); ?></div>
                        <div class="product__price">Цена: <?php echo $product->get_price(); ?> $</div>
                        <a class="btn_blue product__btn-buy" href="?add-to-cart=<?php echo esc_attr( $product->get_id() );?>">Купить</a>
                    </div>                    
                </div>
            </div>
            <!-- end product -->
        </div>
        <!-- end container -->
}


<?php get_footer(); ?>


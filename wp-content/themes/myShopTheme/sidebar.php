<?php
  $all_categories = getWallpaperCategories();
?>
<!-- begin sidebar  -->
            <div class="sidebar sidebar-menu">
                <h4 class="sidebar-menu__title">ФОТООБОИ</h4>
                <ul>
                    <?php 
                    if($all_categories) {
                        foreach ($all_categories as $cat) {
                            if($cat->category_parent == 0) {
                                $category_id = $cat->term_id;       
                                echo '<li><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a></li>';               
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
<!-- end sidebar -->
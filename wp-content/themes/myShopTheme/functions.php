<?php 

//enquery css and scripts
function my_theme_enqueue_scripts() {
	//wp_enqueue_script('jquery_script', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js');
	wp_enqueue_style( 'main_styles', get_template_directory_uri() . '/css/main.min.css' );
   	wp_enqueue_style( 'vendors', get_template_directory_uri() . '/css/vendors.min.css' );
    wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' );
    wp_enqueue_style('admin', get_template_directory_uri() .'/css/admin.css');
	//wp_enqueue_script('main_scripts', get_template_directory_uri() . '/js/main.min.js');
	//wp_enqueue_script('vendors_script', get_template_directory_uri() . '/js/vendors.min.js');	
	

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts' );




//hooking in WooCommerce
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  echo '<section id="main">';
}

function my_theme_wrapper_end() {
  echo '</section>';
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
//end hoking in WooCommerce


//customize checkout fields 
function custom_override_checkout_fields( $fields ) {
    //unset unnessesary billing fields
	  $fields['billing']['billing_company']['required'] = false;
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_state']);
    $fields['billing']['billing_postcode']['required'] = false;
    unset($fields['billing']['billing_postcode']);
    $fields['billing']['billing_city']['required'] = false;
    unset($fields['billing']['billing_city']);
    unset($fields['billing_address_1']);
    $fields['billing']['billing_address_1']['required'] = false;
    $fields['billing']['billing_country']['required'] = false;
    unset($fields['billing']['billing_country']);

    //unset unnessesary shipping fields
    $fields['shipping']['shipping_first_name']['required'] = false;
    unset($fields['shipping']['shipping_first_name']);
    $fields['shipping']['shipping_last_name']['required'] = false;
    unset($fields['shipping']['shipping_last_name']);
    unset($fields['shipping']['shipping_company']);
    $fields['shipping']['shipping_country']['required'] = false;
    unset($fields['shipping']['shipping_country']);
    $fields['shipping']['shipping_state']['required'] = false;
    unset($fields['shipping']['shipping_state']);
    $fields['shipping']['shipping_postcode']['required'] = false;
    unset($fields['shipping']['shipping_postcode']);



    $fields['billing']['billing_first_name']['label'] = 'Имя';
    $fields['billing']['billing_last_name']['label'] = 'Фамилия';
    $fields['billing']['billing_email']['label'] = 'Электронная почта';
    $fields['billing']['billing_phone']['label'] = 'Телефон';

    $fields['shipping']['shipping_city']['label'] = 'Город';
    $fields['shipping']['shipping_address_1']['label'] = 'Улица';
    $fields['shipping']['shipping_address_2']['label'] = 'Дом';
    $fields['shipping']['shipping_adress_3'] = array(
        'label'     => __('Квартира', 'woocommerce'),
    'placeholder'   => _x('Квартира', 'placeholder', 'woocommerce'),
    'required'  => false,
    'class'     => array('form-row-wide'),
    'clear'     => true
     );



     return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );


function getWallpaperCategories($count = null) {

    $taxonomy     = 'product_cat';
    $orderby      = 'name';  
    $show_count   = 0; 
    $pad_counts   = 0;      
    $hierarchical = 1;      
    $title        = '';  
    $empty        = 0;
    $number = $count;

    $args = array(
           'taxonomy'     => $taxonomy,
           'orderby'      => $orderby,
           'show_count'   => $show_count,
           'pad_counts'   => $pad_counts,
           'hierarchical' => $hierarchical,
           'title_li'     => $title,
           'hide_empty'   => $empty,
           'number'       => $number
    );
    return get_categories( $args );
   
}

//acf Option Page support
if( function_exists( 'acf_add_options_page' ) ) {  
    acf_add_options_page();  
  }
add_action( 'init', 'acf_add_options_page' );
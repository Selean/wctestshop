       
        <!-- begin why  -->
        <div class="why">
            <div class="container">
                <div class="why__item">
                    Почему <br>
                    наши фотообои <br>
                    лучшие
                </div>
                <div class="why__item">
                    <div class="why__col"><img src="<?php bloginfo('template_url'); ?>/images/why/why1.jpg" alt=""></div>
                    <div class="why__col">
                        <div class="why__title">Флизелиновая <br> основа</div>
                        <div class="why__content">
                            Клеить легко. <br>
                            Клей наносится на стену. <br>
                            Обои не «разлазятся». <br>
                            Легко и удобно клеятся.
                            Отсутствие горизонтальных
                            стыков делает поклеику
                            максимально удобнои.
                            </div>
                    </div>
                </div>
                <div class="why__item">
                    <div class="why__col"><img src="<?php bloginfo('template_url'); ?>/images/why/why2.jpg" alt=""></div>
                    <div class="why__col">
                        <div class="why__title">Универсальные размеры</div>
                        <div class="why__content">
                            Для больших и маленьких
                            помещений. <br>
                            Легко подогнать под размер стены
                            Не более 3 частей, отсутствие
                            горизонтальных стыков.
                            </div>
                    </div>
                </div>
                <div class="why__item">
                    <div class="why__col"><img src="<?php bloginfo('template_url'); ?>/images/why/why3.jpg" alt=""></div>
                    <div class="why__col">
                        <div class="why__title">Экологично</div>
                        <div class="why__content">
                            При нанесении изображения
                            применяется одна из самых
                            экологичных и современных
                            технологии печати
                            с использованием чернил
                            на воднои основе и латекса
                            Фотообои можно использовать
                            в помещениях для детей
                            и детских садах.
                            Латексные краски — самая
                            экологичная и безопасная печать.
                            Из латекса делают подушки,
                            соски, капельницы.
                            </div>
                    </div>
                </div>
                <div class="why__item">
                    <div class="why__col"><img src="<?php bloginfo('template_url'); ?>/images/why/why4.jpg" alt=""></div>
                    <div class="why__col">
                        <div class="why__title">Горячее тиснение</div>
                        <div class="why__content">
                            Прочные и долговечные.
                            Можно мыть.
                            Красивая текстурная поверхность
                            Вы можете не переживать
                            о долговечности рисунка
                            и его красочности  погодо-
                            и светоустойчивы
                            </div>
                    </div>
                </div>
                <div class="why__item">
                    <div class="why__col"><img src="<?php bloginfo('template_url'); ?>/images/why/why5.jpg" alt=""></div>
                    <div class="why__col">
                        <div class="why__title">Яркость и реалистичность</div>
                        <div class="why__content">
                            ТОП ФОТООБОИ
                            превосходят четкостью.
                            Качественная и четкая печать
                            Все изображения имеют высокую
                            четкость и глубину цвета,
                            что делает картинку яркой
                            и живой.
                            Мы используем только самые
                            новые и тщательно отобранные
                            фотографии молодых
                            дизайнеров и фотографов.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end why -->

        <!-- begin info  -->
        <div class="footer-info">
            <div class="container">
                <p>Не нашли, что искали? <br>
                    Может быть вы просто что-то пропустили. <br>
                    Посмотрите наш каталог еще раз. </p>
                <a class="footer-info__link" href="/shop">Вернуться в каталог</a>
            </div>
        </div>
        <!-- end info -->
        <!-- begin footer  -->
        <div class="footer">
            <div class="container">
                <div class="footer__item footer__menu">
                    <ul>
                        <li><a href="">Инструкция по оклейке</a></li>
                        <li><a href="">Контакты</a></li>
                    </ul>
                </div>
                <div class="footer__item social-menu">
                    <span class="social-menu__title">Поделиться</span>
                    <ul>
                        <li><a href="" target="_blank"><img src="" alt=""><img src="../images/social/vk.png" alt=""></a></li>
                        <li><a href="" target="_blank"><img src="" alt=""><img src="../images/social/f.png" alt=""></a></li>
                        <li><a href="" target="_blank"><img src="" alt=""><img src="../images/social/ok.png" alt=""></a></li>
                        <li><a href="" target="_blank"><img src="" alt=""><img src="../images/social/twitter.png" alt=""></a></li>
                        <li><a href="" target="_blank"><img src="" alt=""><img src="../images/social/google.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end footer -->


    </div>
    <!-- end wrap -->
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
   <script src="<?php bloginfo('template_url'); ?>/js/vendors.min.js"></script>
   <script src="<?php bloginfo('template_url'); ?>/js/main.min.js"></script> 
   
<?php wp_footer(); ?>
</body>
</html>
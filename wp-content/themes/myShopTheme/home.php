<?php get_header(); ?>
        <!-- begin home-slider  -->
        <div class="home-slider container js-home-slider">
            <?php 
                $slides = get_field('slider','options');
                if($slides) {
                    foreach ($slides as $slide) {
                        echo '<div class="item"><img src="'.$slide['url'].'" width="1200" height="600" alt="slide"></div>';
                    }
                }
            ?>
        </div>
        <!-- end home-slider -->

        <h1 class="title"><span>популярные фотообои</span></h1>

        <!-- begin popular-wallpapers  -->
        <div class="wallpapers container">
            <?php 
                $all_categories = getWallpaperCategories(6); 
                print_r($all_categories);
                if ($all_categories) {


                    foreach ($all_categories as $category) { ?>
                        <div class="wallpapers__item">
                            <img src="<?php bloginfo('template_url'); ?>/images/popular/pw2.jpg" alt="">
                            <div class="wallpapers__info">
                                <div class="wallpapers__info-item wallpapers__title"><?php echo $category->name; ?></div>
                                <div class="wallpapers__info-item">
                                    <a class="wallpapers__link" href="<?php echo get_term_link($category->slug, 'product_cat'); ?>">Посмотреть</a>
                                </div>
                            </div>
                        </div>
        <?php       }
                }
            ?>

            
            
            <a class="go-directory" href="/shop">Перейти в каталог</a>
        </div>
        <!-- end popular-wallpapers -->


<?php get_footer(); ?>
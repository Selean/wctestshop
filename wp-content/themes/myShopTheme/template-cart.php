<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Cart
 *
 * 
 */

wp_head(); ?>

<?php 
the_post();
the_content();
get_footer();

?>
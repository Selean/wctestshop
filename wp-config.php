<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'myShop');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0?>G^jAx06;{Wd?ZX_86LHqK~[ibWg=r5%-U_dh=3um]1sIcRpA+--^B/J/YY#Pv');
define('SECURE_AUTH_KEY',  'Q_R-R6JL(M4tr1x8yE9l!K=a>k_ `S`RRFEU4qaA{Nc,BEu)qDnBL2]I|+mg^ky~');
define('LOGGED_IN_KEY',    'Ko1<~|SG rP]U]+j(O?!(&_(3!gS`2f`^Ng|1(Y3Wq QU+[6]a4?5V<TRx(I_`]Y');
define('NONCE_KEY',        'sn`{mpnT;3C[GNPqz6+F6K,@upe.0z@w [fbo pB R{r#xrezq?/Y$oL=;tQxI:5');
define('AUTH_SALT',        '+XM/6s8,7i39{iaD rSr$KRhgC;iO;zB7CQMoBo6d@^ =BvE+K}.G,)s/Q.#G;wF');
define('SECURE_AUTH_SALT', 'H>l-iu?liPB;M8/Pn%dn,dm+>RT+;,uE|0<16Y:D4e1^Q~,0lC%e2-QGADaip_oJ');
define('LOGGED_IN_SALT',   '~D(:D@bSh`ImhQu3,r}1jKj5@Z[^Y.6J-J-jk}pwldH/x;8m@=d%<-.}$ ct~84<');
define('NONCE_SALT',       ';rHwSDUYNyb Z+a9mwMG$b3|?@;jPS)PBXm;N(tc51uJ0mE!:gu:`;vl>4L0ycps');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'flgjko_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
